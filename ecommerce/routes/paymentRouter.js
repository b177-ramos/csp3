const router = require('express').Router()
const paymentCtrl = require('../controllers/paymentCtrl')
const auth = require('../auth/auth')
const authAdmin = require('../auth/authAdmin')


router.route('/payment')
    .get(auth, authAdmin, paymentCtrl.getPayments)
    .post(auth, paymentCtrl.createPayment)


module.exports = router